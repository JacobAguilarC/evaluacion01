/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.model;

/**
 *
 * @author Jacob Aguilar
 */
public class Calculadora {

    private float capital;
    private float anios;
    private float interés;

    /**
     * @return the capital
     */
    public float getCapital() {
        return capital;
    }

    /**
     * @param capital the capital to set
     */
    public void setCapital(float capital) {
        this.capital = capital;
    }

    /**
     * @return the años
     */
    public float getAnios() {
        return anios;
    }

    /**
     * @param años the años to set
     */
    public void setAnios(float años) {
        this.anios = años;
    }

    /**
     * @return the interés
     */
    public float getInterés() {
        return interés;
    }

    /**
     * @param interés the interés to set
     */
    public void setInterés(float interés) {
        this.interés = interés;
    }

    public void calcular() {
        this.interés = (float) (this.capital * 0.1 * this.anios);
    }

}
