<%-- 
    Document   : index
    Created on : 06-may-2021, 13:54:38
    Author     : Jacob Aguilar
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Calculo interés simple</title>
    </head>
    <body>
        <form name="form" action="CalculadoraController" method="POST">
            <section>
                <div>
                    <label>Ingrese capital: </label>
                    <input type="number" name="capital">
                </div>
                
                <div>
                    <label>Ingrese número de años</label>
                    <input type="number" name="anios"> 
                </div>
                
                <div>
                    <button type="submit" name="calcular", value="calcular">Calcular</button>
                </div>
                
            </section>
            
        </form>
    </body>
</html>
