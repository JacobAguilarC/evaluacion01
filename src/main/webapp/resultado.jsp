<%-- 
    Document   : resultado
    Created on : 06-may-2021, 16:19:26
    Author     : Jacob Aguilar
--%>
<%@page import="cl.model.Calculadora"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<%
    Calculadora cal = (Calculadora) request.getAttribute("Calculadora");
%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Resultado</title>
    </head>
    <body>
        <label>Total interés simple: <%= cal.getInterés()%></label>
    </body>
</html>
